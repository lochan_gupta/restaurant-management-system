#Project Title- Restaurant Management System.
	This project is created for easy implementation of restaurant management.This project is created python using Django Framework and tested on Sqlite Database.

##Prereqiusite:

     -Python 2.7
	 -Django 1.8
	 -Postman
	 -Sqlite Database


##Functionalities:

	User can search restaurant according city.
	User can get restaurant by seaching restaurant name.
	User can get restaurant information.
	User can get dishes information for any perticular restaurant.
	User can order for any dishes on any perticular restaurant by using Postman.
	User can get his/her total cost of ordered dish by getting paymant part.

##APIs Implemented:

	-GET /searchbycity/<searchbycity>  http://127.0.0.1:8000/city/<city>
	-GET /reastaurantinfo/<restaurant_info>  http://127.0.0.1:8000/restinfo/<restinfo>
	-GET /dishinfo/<dishinfo>  http://127.0.0.1:8000/dishinfo/<dishinfo>
	-POST /totalpriceoforder/<totalpriceofinfo>.  http://127.0.0.1:8000/total_price/<total_price>

##Running the Project:

	To run the code just open terminal and go to the directory where the code folder is there . 
	Run server by using command [python manage.py runserver]
	There are certain urls provided to run certain urls as mentioned above.