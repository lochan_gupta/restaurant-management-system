# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-07-13 17:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurantapp', '0011_auto_20180713_1338'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='phone_no',
            field=models.CharField(default='1', max_length=11),
        ),
    ]
