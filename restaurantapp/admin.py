# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Restaurant
from .models import Menu
from .models import Discount
from .models import Order

admin.site.register(Restaurant)
admin.site.register(Menu)
admin.site.register(Discount)
admin.site.register(Order)
# Register your models here.
