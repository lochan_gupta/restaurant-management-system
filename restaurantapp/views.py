# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from .models import Discount, Menu, Restaurant,Order
from django.http import HttpResponse
from django.http import QueryDict
from django.core.exceptions import ObjectDoesNotExist
import json

class Search(View):
    def get(self, request, city):
        city = Restaurant.objects.filter(city = city)
        if city.exists():
            result = []
            res = []
            for st in city:
                   res.append(str(st))
            result.append({
                                'Restaurants': res
                  })
            return HttpResponse(json.dumps(result),content_type='application/json')
        else:
            return HttpResponse("City does not found")



class RestaurantInfo (View):
    def get(self, request, name_of_restaurant):
        #try:
            obj = Restaurant.objects.filter(name_of_restaurant=name_of_restaurant)
            # temp = 0
            # for ob in obj:
            #     if  ob.name_of_restaurant == name_of_restaurant:
            #         temp =1
            #         break
            if obj.exists():
                result = []
                res = []
                for restaurant in obj:
                    if restaurant.name_of_restaurant == name_of_restaurant:
                        result.append({
                                        'Restaurant Name':restaurant.name_of_restaurant,
                                        'City':restaurant.city,
                                        'Address':restaurant.Address,
                                        'Contact No.':restaurant.phone_no
                        })

                for restaurant in obj:
                     if restaurant.name_of_restaurant == name_of_restaurant:
                         for menu in restaurant.menu.all():
                              res.append(menu.name_of_dish)

                result.append({
                                  'menus': res,
                    })
                return HttpResponse(json.dumps(result),content_type='application/json')
            else:

                return HttpResponse("Restaurant does not found")


class DishInfo(View):
    def get(self, request, name_of_dish):
            obj = Menu.objects.filter(name_of_dish = name_of_dish)
            # temp =0
            # for ob in obj:
            #     if ob.name_of_dish == name_of_dish:
            #         temp=1
            #         break
            if obj.exists():
                result = []
                for dish in obj:
                      if dish.name_of_dish == name_of_dish:
                          result.append({
                                        'Name of dish ': dish.name_of_dish,
                                        'Price ': dish.price,
                                        'Description': dish.description
                          })
                return HttpResponse(json.dumps(result),content_type='application/json')
            else:
                return HttpResponse("Menu does not exists")


class Order(View):
        def post(self,request):
         try:
            name_of_restaurant = request.POST["restaurant"]

            rest = Restaurant.objects.get(name_of_restaurant = name_of_restaurant)
            rest_Id = rest.id

            name_of_dish = request.POST["dish"]
            dish = Menu.objects.get(name_of_dish = name_of_dish)
            dish_Id = dish.id

            customer_name = request.POST["customer"]
            Menu_objects = Menu.objects.all()
            for dish in Menu_objects:
                if dish.name_of_dish == name_of_dish:
                    partial_price = dish.price
                    break

            discounts =Discount.objects.all()
            for dis in discounts:
                if dis.offer_tnc < partial_price:
                      discount = dis.offer_in_amount
                      partial_price = partial_price -  dis.offer_in_amount
                      break

            total_price = partial_price
            result = []
            disc = Discount.objects.get(offer_in_amount = discount)

            order_detail = Order(restaurant_name = rest_Id, dishes = dish_Id, total_price = total_price,dicount = dish.id, customer_name = customer_name)
            # print(order_detail)
            # order_detail.save()
            result.append({
                          'Thank You for your order.' : customer_name,
                          'You got discount of rupees ': str(discount),
                          'Your total cost is ': str(total_price)
            })
            return HttpResponse(json.dumps(result),content_type='application/json')
         except Exception as IntegrityError:
            return HttpResponse("Integrity Error")
