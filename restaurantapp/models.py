# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Discount(models.Model):

    offer_code = models.CharField(max_length = 30)
    offer_tnc = models.IntegerField()
    offer_in_amount = models.IntegerField(default =1 )
    def __str__(self):
             return (str(self.offer_tnc))

class Menu(models.Model):
    name_of_dish = models.CharField(max_length = 20)
    price = models.IntegerField()
    description  = models.CharField(max_length = 100)
    def __str__(self):
        return (str(self.name_of_dish))

class Restaurant(models.Model):
    menu = models.ManyToManyField(Menu)
    discount = models.ManyToManyField(Discount)
    name_of_restaurant = models.CharField(max_length = 30)
    longitude = models.CharField(max_length = 15)
    latitude = models.CharField(max_length=15)
    city = models.CharField(max_length = 20,default ='a')
    discount_code = models.CharField(max_length=30)
    Address = models.CharField(max_length = 200)
    phone_no = models.CharField(max_length =11,default ='1')
    #information = models.CharField(max_length = 500)
    def __str__(self):
        return (str(self.name_of_restaurant))

class Order(models.Model):
    #city = models.ForeignKey(Restaurant,on_delete=models.CASCADE)
    orderid = models.AutoField(primary_key=True)
    restaurant_name = models.ForeignKey(Restaurant,on_delete=models.CASCADE)
    dishes = models.ForeignKey(Menu)
    total_price = models.IntegerField()
    discount = models.ForeignKey(Discount,on_delete=models.CASCADE, default=1)
    customer_name = models.CharField(max_length = 50)








#      def __str__(self):
#          return (str(self.offer_tnc))



# Create your models here.
